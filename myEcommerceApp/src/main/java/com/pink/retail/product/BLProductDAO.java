package com.pink.retail.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.pink.retail.display.DisplayData;

public class BLProductDAO implements BLIProductDAO {
	
	private DisplayData displayData;
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private EntityTransaction productTransaction;
	
	public BLProductDAO() {
		displayData = new DisplayData();
		entityManagerFactory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public ProductEntity getProductByID(int productID) {
		productTransaction = entityManager.getTransaction();
		productTransaction.begin();
		if (entityManager.find(ProductEntity.class, productID) == null) {
			displayData.message("The product does not exist!");
		}
		ProductEntity product = entityManager.find(ProductEntity.class, productID);
		productTransaction.commit();
		return product;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductEntity> getAllProducts() {
		productTransaction = entityManager.getTransaction();
		productTransaction.begin();
		List<ProductEntity> products = new ArrayList<>();
		String sql = "SELECT u from ProductEntity u";
		Query query = entityManager.createQuery(sql);
		products = query.getResultList();
		productTransaction.commit();
		
		return products;
	}

	@Override
	public void deleteProductByID(int productID) {
		productTransaction = entityManager.getTransaction();
		productTransaction.begin();
		if (entityManager.find(ProductEntity.class, productID) == null) {
			displayData.message("The product does not exist!");
		} else {
			ProductEntity productToDelete = entityManager.getReference(ProductEntity.class, productID);
			entityManager.remove(productToDelete);
		}
		productTransaction.commit();
	}

	@Override
	public void deleteAllProducts() {
		productTransaction = entityManager.getTransaction();
		productTransaction.begin();
		String sql = "DELETE from ProductEntity";
		Query query = entityManager.createQuery(sql);
		query.executeUpdate();
		entityManager.getTransaction().commit();
	}

	@Override
	public void insertProduct(ProductEntity product) {
		productTransaction = entityManager.getTransaction();
		productTransaction.begin();
		if (entityManager.contains(product)) {
			displayData.message("The database already contains the product you try to insert");
		} else {
			entityManager.persist(product);
		}
		entityManager.getTransaction().commit();
	}

	@Override
	public void updateProduct(ProductEntity product) {
		productTransaction = entityManager.getTransaction();
		productTransaction.begin();
		if(entityManager.find(ProductEntity.class, product.getProductID())==null) {
			displayData.message("The database does not contain the product you try to update");
		} else {
			entityManager.merge(product);
		}
		productTransaction.commit();
	}

}
