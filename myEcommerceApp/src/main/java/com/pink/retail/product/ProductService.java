package com.pink.retail.product;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/product")
public class ProductService {
	
	private BLIProductDAO productDao;

	@GET
	@Path("/{productID}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductEntity getProductByID(@PathParam("productID") int productID) {
		productDao = new BLProductDAO();
		return productDao.getProductByID(productID);
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductEntity> getProducts() {
		productDao = new BLProductDAO();
		return productDao.getAllProducts();
	}
	
	 @DELETE
	 @Path("/delete/{productID}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void deleteProduct(@PathParam("productID") int productID) {
		 productDao = new BLProductDAO();
		 productDao.deleteProductByID(productID);
	 }
	 
	 @PUT
	 @Path("/add")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void addProduct(ProductEntity product) {
		 productDao = new BLProductDAO();
		 productDao.insertProduct(product);
	  }
	 
	 @POST
	 @Path("/updateBrand/{productID}/{productBrand}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void updateProductBrand(ProductEntity product, @PathParam("productID") int productID, @PathParam("productBrand") String productBrand) {
		 productDao = new BLProductDAO();
		 product.setBrand(productBrand);
		 productDao.updateProduct(product);
	  }
	 
	 @POST
	 @Path("/updateProduct")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void updateProduct(ProductEntity product) {
		 productDao = new BLProductDAO();
		 productDao.updateProduct(product);
	  }

}
