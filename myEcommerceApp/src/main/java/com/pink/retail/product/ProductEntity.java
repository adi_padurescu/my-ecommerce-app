package com.pink.retail.product;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.pink.retail.order.OrderEntity;

/**
 * Entity implementation class for Entity: Product
 *
 */
@Entity
@Table(name = "PRODUCT")
@XmlRootElement
@XmlAccessorType(value = XmlAccessType.FIELD)
public class ProductEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID")
	private int productID;

	@Column(name = "NAME")
	private String name;
	
	@Column(name = "BRAND")
	private String brand;

	@Column(name = "QUANTITY")
	private int quantity;

	@Column(name = "PRICE")
	private double price;

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "listProduct")
	@XmlTransient
	private List <OrderEntity> orders;
		
	public ProductEntity(String name, String brand, int quantity, double price) {
		super();
		this.name = name;
		this.brand = brand;
		this.quantity = quantity;
		this.price = price;
	}

	public ProductEntity() {
		super();
	}
	
	public List<OrderEntity> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderEntity> orders) {
		this.orders = orders;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		if (quantity == 0) {
			return name + " "+ brand + " - Sold Out ! ";
		} else {
			return productID + "." + name + " "+ brand + " Quantity:" + quantity + " Price/Item: " + price + " RON";
		}
	}

}
