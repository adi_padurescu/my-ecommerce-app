package com.pink.retail.product;

import java.util.List;


public interface BLIProductDAO {
	
	ProductEntity getProductByID (int productID);
	List <ProductEntity> getAllProducts();
	void deleteAllProducts();
	void insertProduct(ProductEntity product);
	void updateProduct(ProductEntity product);
	void deleteProductByID(int productID);
	
}
