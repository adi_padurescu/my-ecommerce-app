package com.pink.retail.product;
/**
 * Class where we return the list of products for a firm and has method for removing products from firm product list
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.pink.retail.util.Utility;

public class ProductStock {
	
	private List<Product> listProduct;
	
	public ProductStock() throws IOException
	{
		listProduct=new ArrayList<Product>();
		BLIProductReader aux=new BLProductReader();
		listProduct=aux.readListProduct(Utility.LIST_OF_PRODUCT_PATH);	
	}

	public List<Product> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<Product> listProduct) {
		this.listProduct = listProduct;
	} 
	
	public int removeItemFromProductList(ProductStock p, int index, int quantityCh)
	{
		List<Product> Products=p.getListProduct();
		if(Products.get(index-1).getQuantity()<0 || Products.get(index-1).getQuantity()-quantityCh<0)
		{
			return 0;
		}
		else
		{
			int aux=Products.get(index-1).getQuantity()-quantityCh;
			Products.get(index-1).setQuantity(aux);
			p.setListProduct(Products);
			return 1;
		}
	}
	//Method for closing the shop
		public int checkForClosingShop(ProductStock firm_stock)
		{
			List<Product> auxFirmStock=new ArrayList<Product>();
			auxFirmStock=firm_stock.getListProduct();
			int sumOfQuantity=0;
			for(int i=0; i<auxFirmStock.size(); i++)
			{
				sumOfQuantity+=auxFirmStock.get(i).getQuantity();
			}
			if(sumOfQuantity==0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
}
