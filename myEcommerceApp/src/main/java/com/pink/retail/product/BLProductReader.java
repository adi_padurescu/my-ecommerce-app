package com.pink.retail.product;

/**
 * Class for reading a list of product from a file
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class BLProductReader implements BLIProductReader {

	public List<Product> readListProduct(String path) throws IOException
	{
		List<Product> productList=new ArrayList<Product>();
		String[] parts;
		FileInputStream fstream = new FileInputStream(path);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
			parts=strLine.split(",");
			String nameProduct=parts[0];
			double priceProduct=Double.parseDouble(parts[1]);
			int quantityProduct=Integer.parseInt(parts[2]);
			productList.add(new Product(nameProduct,quantityProduct,priceProduct));
		}
		br.close();
		return productList;
		
	}
}
