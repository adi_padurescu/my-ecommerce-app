package com.pink.retail.product;
/**
 * Here is the class where you manage the ShoppingCart for a user
 * Contains methods for adding a product to ShoppingCart or calculating the price
 */
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
	
	public double currentePrice(double prices,int index, int quantity, ProductStock listProduct)
	{
		prices+=quantity*listProduct.getListProduct().get(index-1).getPrice();
		prices=Math.round(prices * 100.0) / 100.0;
		return prices;
	}
	
	//method for adding a product to Client Shopping Cart
	public void addItemToCart(List<Integer> index_buyer, List<Integer> quantity_buyer, int index, int quantity)
	{
 		int aux_opt=1; // If the product is already exist in the list, then we update the quantity
    	for(int i=0; i<index_buyer.size();i++)
    	{
    		if(index==index_buyer.get(i))
    		{
    			aux_opt=2;
    			int aux_quantity_bought=quantity_buyer.get(i);
    			aux_quantity_bought+=quantity;
    			quantity_buyer.set(i, aux_quantity_bought);
    		}
    	}
    	if(aux_opt==1) // If the product wasn't choose already, we insert it in the list of client Shopping Cart
    	{
    		index_buyer.add(index);
    		quantity_buyer.add(quantity);
    	}
	}
	
	//If the payment is refused, the modify of list are undo
	public void commandCancel(List<Integer> index_buyer, List<Integer> quantity_buyer, ProductStock firm_stock)
	{ 
		List<Product> aux=new ArrayList<Product>();
		aux=firm_stock.getListProduct();
		for(int i=0; i<index_buyer.size(); i++)
		{
			int aux_quantity=aux.get(index_buyer.get(i)-1).getQuantity();
			aux_quantity+=quantity_buyer.get(i);
			aux.get(index_buyer.get(i)-1).setQuantity(aux_quantity);
		}
		firm_stock.setListProduct(aux);
	}
	
	//Method for validation input values
	public int validationInputValues(int inputValue, ProductStock firm_stock)
	{
		if (inputValue > firm_stock.getListProduct().size() || inputValue<=0 || firm_stock.getListProduct().get(inputValue-1).getQuantity()==0) {
			return 0;
		} else {
			return 1;
		}
	}
	
	//Method for validation input quantity products
	public int validationInputQuantity(int inputQuantity, int inputIndex, ProductStock firm_stock)
	{
		if(firm_stock.getListProduct().get(inputIndex-1).getQuantity()<inputQuantity)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}
