package com.pink.retail.product;
/**
 *Creating Product with name, quantity and price fields
 */

public class Product {
	
	private String name;
	private int quantity;
	private double price;
	
	public Product()
	{
		this.name="";
		this.quantity=0;
		this.price=0;
	}
	public Product(String name, int quantity, double price) {
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String toString()
	{
		if (quantity == 0) {
			return name + " - Sold Out ! ";
		} else {
			return name + " Quantity:" + quantity + " Price/Item: " + price + " lei";
		}
	}

}
