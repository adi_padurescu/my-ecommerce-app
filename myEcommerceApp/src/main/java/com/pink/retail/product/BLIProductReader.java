package com.pink.retail.product;
/**
 * interface with the method for reading a Product list from a file 
 */
import java.io.IOException;
import java.util.List;

public interface BLIProductReader {
	
	List<Product> readListProduct(String path) throws IOException;
}
