package com.pink.retail.order;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/order")
public class OrderService {
	private BLOrderDAO orderDAO;
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrderEntity> getAllOrder() {
		orderDAO = new BLOrderDAO();
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		orderList = orderDAO.findAllOrders();
		return orderList;
	}
	
	@GET
	@Path("/{orderID}")
	@Produces(MediaType.APPLICATION_JSON)
	public OrderEntity getOrderByID(@PathParam("orderID") int orderID) {
		orderDAO = new BLOrderDAO();
		return orderDAO.findOrdersByID(orderID);
	}
	
	@PUT
	@Path("/addOrder")
	@Produces(MediaType.APPLICATION_JSON)
	public void insertOrder(OrderEntity orderEntity) {
		orderDAO = new BLOrderDAO();
		orderDAO.insertOrders(orderEntity);
	}
	
	@POST
	@Path("/updateOrder/{orderID}")
	@Produces(MediaType.APPLICATION_JSON)
	public void updateOrder(OrderEntity orderEntity, @PathParam("orderID") int orderID) {
		orderDAO = new BLOrderDAO();
		orderEntity.setOrderID(orderID);
		orderDAO.updateOrders(orderEntity);
	}
	
	@DELETE
	@Path("/deleteOrder/{orderID}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteOrder(@PathParam("orderID") int orderID) {
		orderDAO = new BLOrderDAO();
		orderDAO.deleteByIdOrders(orderID);
	}
}