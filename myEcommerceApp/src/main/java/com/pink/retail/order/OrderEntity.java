package com.pink.retail.order;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.pink.retail.customer.CustomerEntity;
import com.pink.retail.product.ProductEntity;

@XmlRootElement
@Entity
@Table(name = "ORDERS")
public class OrderEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORDER_ID")
	private int orderID;

	@Column(name = "ORDER_DATE")
	private String orderDate;

	@Column(name = "ORDER_STATUS")
	private String orderStatus;
	
	@Column(name = "PAYMENT_METHOD")
	private String paymentMethod;

	@ManyToMany(mappedBy = "orders", cascade = CascadeType.PERSIST)
	private List<ProductEntity> listProduct;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "customerId")
	private CustomerEntity customer;

	public OrderEntity() {
		super();
	}
	
	public OrderEntity(String orderDate, String orderStatus,String paymentMethod) {
		this.orderDate = orderDate;
		this.orderStatus = orderStatus;
		this.paymentMethod = paymentMethod;
	}
	
	public OrderEntity(int orderID, String orderDate, String orderStatus, String paymentMethod) {
		this.orderID = orderID;
		this.orderDate = orderDate;
		this.orderStatus = orderStatus;
		this.paymentMethod = paymentMethod;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public List<ProductEntity> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<ProductEntity> listProduct) {
		this.listProduct = listProduct;
	}

	public CustomerEntity getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "\nOrderID: " + this.orderID + "\nOrderDate: " + orderDate + "\nOrderStatus: " + orderStatus
				+ "\nPaymentMethod: " + paymentMethod + "\n";
	}
}
