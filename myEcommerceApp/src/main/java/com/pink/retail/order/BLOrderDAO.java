package com.pink.retail.order;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.pink.retail.display.DisplayData;
import com.pink.retail.util.Utility;

public class BLOrderDAO implements BLIOrderDAO{
	private DisplayData displayMessage;
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory; 

	public BLOrderDAO() {
		displayMessage = new DisplayData();
		entityManagerFactory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public OrderEntity findOrdersByID(int idOrdersSearch) {
		displayMessage.message(Utility.ORDER_FIND_BY_ID + idOrdersSearch);
		EntityTransaction findOrdersTransaction = entityManager.getTransaction();
		findOrdersTransaction.begin();
		OrderEntity order = entityManager.find(OrderEntity.class, idOrdersSearch);
		findOrdersTransaction.commit();
		return order;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderEntity> findAllOrders() {
		EntityTransaction findAllOrdersTransaction = entityManager.getTransaction();
		findAllOrdersTransaction.begin();
		Query query = entityManager.createQuery("SELECT e FROM OrderEntity e");
		List<OrderEntity> listOfOrders = new ArrayList<OrderEntity>(query.getResultList());
		displayMessage.message(Utility.ORDER_FIND_ALL);
		findAllOrdersTransaction.commit();
		return listOfOrders;
	}

	@Override
	public void deleteAllOrders() {
		EntityTransaction deleteAllOrdersTransaction = entityManager.getTransaction();
		deleteAllOrdersTransaction.begin();
		Query q = entityManager.createQuery("DELETE FROM OrderEntity");
		if(q.executeUpdate() != 0 ) {
			displayMessage.message(Utility.ORDER_DELETE_ALL);
		} else {
			displayMessage.message(Utility.NO_ORDERS);
		}
		deleteAllOrdersTransaction.commit();
	}

	@Override
	public void insertOrders(OrderEntity addOrders) {
		displayMessage.message(Utility.ORDER_INSERT_NEW);
		EntityTransaction insetOrderTransaction = entityManager.getTransaction();
		insetOrderTransaction.begin();
		displayMessage.showOrders(addOrders);
		entityManager.persist(addOrders);
		insetOrderTransaction.commit();		
	}

	@Override
	public void updateOrders(OrderEntity updateOrder) {
		EntityTransaction updateOrderTransaction = entityManager.getTransaction();
		updateOrderTransaction.begin();
		if(entityManager.find(OrderEntity.class, updateOrder.getOrderID()) == null)
		{
			displayMessage.message(Utility.NO_ORDER + updateOrder.getOrderID());	
		}
		else
		{
			displayMessage.message(Utility.ORDER_UPDATE_BY_ID + updateOrder.getOrderID() + Utility.ORDER_NEW_ADDRESS_UPDATE);
			displayMessage.showOrders(updateOrder);
			entityManager.merge(updateOrder);
		}
		updateOrderTransaction.commit();
	}

	@Override
	public void deleteByIdOrders(int idOrderDelete) {
		EntityTransaction deleteOrderIDTransaction = entityManager.getTransaction();
		deleteOrderIDTransaction.begin();
		if(entityManager.find(OrderEntity.class, idOrderDelete) == null)
		{
			displayMessage.message(Utility.NO_ORDER);
		}
		else
		{
			OrderEntity order = entityManager.getReference(OrderEntity.class, idOrderDelete);
			displayMessage.message(Utility.ORDER_DELETE_BY_ID + idOrderDelete);
			entityManager.remove(order);
		}
		deleteOrderIDTransaction.commit();
	}
}
