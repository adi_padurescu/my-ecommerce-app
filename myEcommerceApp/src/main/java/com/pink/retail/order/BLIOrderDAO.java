package com.pink.retail.order;

import java.util.List;

public interface BLIOrderDAO {

	public OrderEntity findOrdersByID(int idOrdersSearch);
	public List<OrderEntity> findAllOrders();
	public void deleteAllOrders();
	public void insertOrders(OrderEntity addOrders);
	public void updateOrders(OrderEntity updateOrders);
	public void deleteByIdOrders(int idOrdersDelete);
}
