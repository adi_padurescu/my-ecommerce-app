package com.pink.retail.entities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory factory;

	public EntityManagerFactory getFactory() {
		return factory;
	}

	public void setFactory(EntityManagerFactory factory) {
		JPAUtil.factory = factory;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		if (factory == null) {
			factory = Persistence.createEntityManagerFactory("EcommerceApp");
		}
		return factory;
	}

	public void shutdown() {
		if (factory != null) {
			factory.close();
		}
	}
	public void readMySqlFromFile(String path, EntityManager entityManager) throws IOException {
		
		FileInputStream fstream = new FileInputStream(path);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		// Read File Line By Line
		entityManager.getTransaction().begin();
		
		while ((strLine = br.readLine()) != null) {
	        entityManager.createNativeQuery(strLine).executeUpdate();
		}
		entityManager.getTransaction().commit();
		br.close();

	}
}
