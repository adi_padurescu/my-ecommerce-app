package com.pink.retail.address;

import java.util.List;

public interface BLIAddressDAO {

	public AddressEntity findAddressByID(int idAddressSearch);
	
	public List<AddressEntity> findAllAddress();
	
	public void deleteAllAddress();
	
	public void insertAddress(AddressEntity addAddress);
	
	public void updateAddress(AddressEntity updateAddress);

	public void deleteByIdAddress(int idAddressDelete);
}
