package com.pink.retail.address;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/address")
public class AddressService {

	private BLAddressDAO addressDAO;
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AddressEntity> getAllAddress() {
		addressDAO = new BLAddressDAO();
		List<AddressEntity> list = new ArrayList<AddressEntity>();
		list =  addressDAO.findAllAddress();
		return list;
	}

	@GET
	@Path("/{address_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public AddressEntity getById(@PathParam("address_id") int address_id) {
		addressDAO = new BLAddressDAO();
		return addressDAO.findAddressByID(address_id);
	}
	
	 @PUT
	 @Path("/addAddress")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void insertAddress(AddressEntity address) {
		 addressDAO = new BLAddressDAO();
	      addressDAO.insertAddress(address);
	  }
	
	 @POST
	 @Path("/updateAddress/{address_id}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void updateAddress(AddressEntity address,@PathParam("address_id") int address_id) {
		 addressDAO = new BLAddressDAO();
		 address.setAddressId(address_id);
	     addressDAO.updateAddress(address);
	  }
	 
	 @DELETE
	 @Path("/deleteAddress/{address_id}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void deleteAddress(@PathParam("address_id") int address_id) {
		 addressDAO = new BLAddressDAO();
		 addressDAO.deleteByIdAddress(address_id);
	 }
}
