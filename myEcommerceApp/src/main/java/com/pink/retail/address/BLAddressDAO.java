package com.pink.retail.address;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.pink.retail.display.DisplayData;
import com.pink.retail.util.Utility;

public class BLAddressDAO implements BLIAddressDAO{
	DisplayData displayMessage=new DisplayData();
	private EntityManager entityManager;
	
	public BLAddressDAO()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = emf.createEntityManager();
	}
	
	public EntityManager getEntityManager() {

		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public AddressEntity findAddressByID(int idAddressSearch) {		
		displayMessage.message(Utility.ADDRESS_FIND_BY_ID_SUCCESS+idAddressSearch);
		EntityTransaction findAddressesTransaction = entityManager.getTransaction();
		findAddressesTransaction.begin();
		AddressEntity address=entityManager.find(AddressEntity.class,idAddressSearch);
		findAddressesTransaction.commit();
		return address;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AddressEntity> findAllAddress() {
		EntityTransaction findAllAddressesTransaction = entityManager.getTransaction();
		findAllAddressesTransaction.begin();
		Query query = entityManager.createQuery("SELECT e FROM AddressEntity e");
		List<AddressEntity> listOfAddress=new ArrayList<AddressEntity>(query.getResultList());
		displayMessage.message(Utility.ADDRESS_FIND_ALL);
		findAllAddressesTransaction.commit();
		 return listOfAddress;
	}

	@Override
	public void deleteAllAddress() {
		EntityTransaction deleteAllAddressesTransaction = entityManager.getTransaction();
		deleteAllAddressesTransaction.begin();
		Query q = entityManager.createQuery ("DELETE FROM AddressEntity");

		if(q.executeUpdate ()!=0)
		{
			displayMessage.message(Utility.ADDRESS_REMOVE_ALL);
		}
		else
		{
			displayMessage.message(Utility.NO_ADDRESS);
		}
		deleteAllAddressesTransaction.commit();
	}

	@Override
	public void insertAddress(AddressEntity addAddress) {
		displayMessage.message(Utility.ADDRESS_INSERT_NEW);
		EntityTransaction insetAddressesTransaction = entityManager.getTransaction();
		insetAddressesTransaction.begin();
		displayMessage.showAddress(addAddress);
		entityManager.persist(addAddress);
		insetAddressesTransaction.commit();
	}

	@Override
	public void updateAddress(AddressEntity updateAddress) {
		EntityTransaction updateAddressesTransaction = entityManager.getTransaction();
		updateAddressesTransaction.begin();
		if(entityManager.find(AddressEntity.class, updateAddress.getAddressId())==null)
		{
			displayMessage.message(Utility.NO_ADDRESS_BY_ID+updateAddress.getAddressId());	
		}
		else
		{
			displayMessage.message(Utility.ADDRESS_UPDATE_BY_ID+updateAddress.getAddressId()+Utility.ADDRESS_NEW_ADDRESS_UPDATE);
			displayMessage.showAddress(updateAddress);
			entityManager.merge(updateAddress);
		}
		updateAddressesTransaction.commit();
	}
	

	@Override
	public void deleteByIdAddress( int idAddressDelete) {
		EntityTransaction deleteAddressesIDTransaction = entityManager.getTransaction();
		deleteAddressesIDTransaction.begin();
		if(entityManager.find(AddressEntity.class, idAddressDelete)==null)
		{
			displayMessage.message(Utility.NO_ADDRESS);
			
		}
		else
		{
			AddressEntity address = entityManager.getReference(AddressEntity.class, idAddressDelete);
			displayMessage.message(Utility.ADDRESS_DELETE_BY_ID + idAddressDelete);
			entityManager.remove(address);
		}
		deleteAddressesIDTransaction.commit();
	}
	
}
