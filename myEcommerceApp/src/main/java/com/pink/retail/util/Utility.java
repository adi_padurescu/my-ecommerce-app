package com.pink.retail.util;

/**
 * Class where we have all constant variable, as path for reading input file or
 * message which we show on Console
 */
public class Utility {

	// Path for reading file
	public final static String LIST_OF_PRODUCT_PATH = "src/main/resources/products.txt";
	public final static String LIST_OF_BUDGET_PATH = "src/main/resources/budget.txt";
	public final static String LIST_OF_INITIAL_MONEY_PATH = "src/main/resources/money.txt";
	public final static String MYSQL_INSERT_ADDRESS = "src/main/resources/address.sql";
	public final static String MYSQL_INSERT_CUSTOMER = "src/main/resources/customer.sql";
	public final static String MYSQL_INSERT_PRODUCT = "src/main/resources/product.sql";

	// Message for buying products
	public final static String BUYING_FIRST_MESSAGE = "List of products: \n";
	public final static String BUYING_SELECT_INDEX_PRODUCT = "\nSelect Product index: ";
	public final static String BUYING_WRONG_INDEX_PRODUCT = "Wrong index. The product doesn't exist or is sold out!\n";
	public final static String BUYING_SELECT_QUANTITY_PRODUCT = "Select Product quantity: ";
	public final static String BUYING_WRONG_QUANTITY_PRODUCT = "You don't have the quantity you want! ";
	public final static String BUYING_CONFIRMATION_MESSAGE = "\nDo you want to buy more?\n\t1-YES\n\t2-NO\n R: ";

	// Message for payments products
	public final static String PAYMENT_NEED_PAY = "You have to pay: ";
	public final static String PAYMENT_SELECT_MONEY = "\nSelect money index: ";
	public final static String PAYMENT_WRONG_INDEX = "Wrong index!\n";
	public final static String PAYMENT_QUANTITY_MONEY = "Select Money quantity: ";

	// Meesage for rest client
	public final static String REST_SUCCESS_PAY = "\tYour payment was a success!\n";
	public final static String REST_BILL_INFO = "\n\t\tBill Information \n";
	public final static String REST_PRODUCT_BOUGHT = "\tThe client has bought the next products: \n";
	public final static String REST_CLIENT_PAYMENT = "\n\tThe client paid with: \n";
	public final static String REST_CLIENT_REST = "\n\tThe client rest is: \n";
	public final static String REST_FIRM_BUDGET = "\n\tThe budget of firm is: \n";
	public final static String REST_CANCEL_PAY = "We don't have change for your payment!\n";

	// Message for foreign value
	public final static String FOREIGN_VALUE_PAY = "\nInsert the value you want to pay: ";

	// Message for client session
	public final static String CLIENT_BALANCE_FIRM = "Balance of firm is: ";
	public final static String CLIENT_PRODUCT_LIST_SELECTED = "\n\tThe client has selected the next products: \n";
	public final static String CLIENT_NEED_TO_PAY = "\tYou need to pay: ";
	public final static String CLIENT_SELECT_CURRENCE = "\nCurrence value: Lei.\n Do you want to exchange? \n\t1-No\n\t2-Yes\nR: ";
	public final static String CLIENT_FIRM_BUDGET = "\nThe firm budget is: ";
	public final static String CLIENT_CHOOSE_NEW_VALUE = "\nChoose new value:\n\t1-EUR \n\t2-USD\nR: ";

	// Message ERROR wrong input
	public final static String BAD_INFO_INPUT_ERROR = "ERROR! You have inserted bad info.";

	// Message For DataBases
	public final static String STATEMENT_DELETE_TABLE_IF_EXISTS = "DROP table if exists products";
	public final static String STATEMENT_CREATE_TABLE_PRODUCTS = "create table if not exists products ("
			+ "id int(50) auto_increment not null," + "name varchar(256) not null," + "price double not null , "
			+ "quantity int(50) not null," + "primary key(id))";
	public final static String STATEMENT_INSERT_TABLE_PRODUCTS = "insert into products(name,price,quantity) values(?,?,?)";
	public final static String STATEMENT_SELECT_FROM_PRODUCT_TABLE = "select name, price, quantity from products";
	public final static String STATEMENT_SELECT_PRODUCT_BY_ID = "select name, price, quantity from products where id=?";
	public final static String STATEMENT_UPDATE_PRODUCT_BY_ID = "UPDATE products SET "
			+ " price = ? , quantity = ? WHERE id=?";
	public final static String STATEMENT_DELETE_PRODUCT = "DELETE from products WHERE id = ?";
	public final static String MESSAGE_SUCCESS_INSERTION = "Successful insertion\n";
	public final static String MESSAGE_PRODUCT_SEARCH_ID = "Product search by ID=";
	public final static String MESSAGE_SUCCESS_UPDATE = "Successful update\n";
	public final static String MESSAGE_PRODUCT_WITH_ID = "The product with ID = ";
	public final static String MESSAGE_SUCCESS_DELETE = " was successfully deleted!";

	// Loggs for DataBase Address
	public final static String ADDRESS_FIND_BY_ID_SUCCESS = "Get addresses by ID: ";
	public final static String ADDRESS_FIND_ALL = "Show all addresses";
	public final static String ADDRESS_REMOVE_ALL = "Remove all addresses";
	public final static String ADDRESS_INSERT_NEW = "Inserted this address: ";
	public final static String ADDRESS_UPDATE_BY_ID = "Update the address with ID ";
	public final static String ADDRESS_NEW_ADDRESS_UPDATE = " by address: ";
	public final static String ADDRESS_DELETE_BY_ID = "Deleted address by ID: ";
	public final static String NO_ADDRESS_BY_ID = "There is no address with the ID: ";
	public final static String NO_ADDRESS = "There are no adresses.";

	// Loggs for Customers
	public final static String CUSTOMER_INSERT_NEW = "Inserted this customer into database: ";
	public final static String CUSTOMER_UPDATE_BY_ID = "Update the customer with ID ";
	public final static String CUSTOMER_NEW_CUSTOMER_UPDATE = " by customer informations: ";
	public final static String CUSTOMER_FIND_BY_ID = "Get customers by ID: ";
	public final static String CUSTOMER_FIND_BY_EMAIL_AND_PASSWORD = "Get customers by email and password: ";
	public final static String CUSTOMER_FIND_ALL = "Get all customers";
	public final static String CUSTOMER_DELETE_ALL = "Deleted all customers";
	public final static String CUSTOMER_DELETE_BY_ID = "Deleted customer by ID: ";
	public final static String NO_CUSTOMER = "There is no customer with the ID: ";
	public final static String NO_CUSTOMERS = "There are no customers";
	
	//Loggs for Orders
	public final static String ORDER_INSERT_NEW = "Inserted this order into database: ";
	public final static String ORDER_UPDATE_BY_ID = "Update the order with ID ";
	public final static String ORDER_NEW_CUSTOMER_UPDATE = " by order informations: ";
	public final static String ORDER_FIND_BY_ID = "Get order by ID: ";
	public final static String ORDER_FIND_ALL = "Get all orders.";
	public final static String ORDER_DELETE_ALL = "Deleted all orders";
	public final static String ORDER_DELETE_BY_ID = "Deleted order by ID: ";
	public final static String ORDER_NEW_ADDRESS_UPDATE = " by order: ";
	public final static String NO_ORDER = "There is no order with the ID: ";
	public final static String NO_ORDERS = "There are no orders.";
}
