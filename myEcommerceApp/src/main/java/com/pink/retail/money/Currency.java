package com.pink.retail.money;
/**
 * Class for client payment with foreign value
 * Methods for changing Value to EUR or USD and dividing a foreign value in main currency Bill and Coins
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Currency {

	public double chooseCurrence(int opt) throws IOException
	{
		double valoare;
		InputStream in = new FileInputStream("src/main/resources/currency.properties");
		Properties prop=new Properties();
		prop.load(in);
			if(opt==1)
			{
				valoare=Double.parseDouble(prop.getProperty("exchangeE"));
			}
			else
			{
				valoare=Double.parseDouble(prop.getProperty("exchange$"));
			}
			return valoare;
	}
	
	public double changeForeignValue(double val,double valInput)
	{
		return val*valInput;
	}
	
	//method for dividing a value to bills and coins
	public void divideNumberInMoney(double value, List<Money> clientPaymentList) throws IOException
	{
		Balance balance_aux=new Balance();
		MoneyStock money_stock_auxiliar=new MoneyStock();
		double aux_value=value;
		List<Double> available_value_money=new ArrayList<Double>();
		available_value_money=money_stock_auxiliar.initMoneyList();
		while(aux_value>0)
		{
			int loc=-1;
			for(int i=0; i<available_value_money.size(); i++)
			{
				if(available_value_money.get(i)<=aux_value)
				{
					loc=i;
				}
			}
			balance_aux.addRest(clientPaymentList, available_value_money.get(loc), 1);
			aux_value-=available_value_money.get(loc);
		}
		
	}
}
