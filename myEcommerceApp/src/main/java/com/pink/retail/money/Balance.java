package com.pink.retail.money;
/**
 * Class for calculating Balance of firm
 * Method for returing the rest, adding money to a rest list etc.
 */
import java.util.ArrayList;
import java.util.List;


public class Balance {
	
	//method for adding and money object to list rest
	public void addRest(List<Money> restList, double value,int quantity)
	{
		int aux_opt=1;
		for(int i=0; i<restList.size(); i++)
		{
			if(value==restList.get(i).getValue())
			{
				aux_opt=2;
				int aux_quantity=restList.get(i).getQuantityMoney()+quantity;
				restList.get(i).setQuantityMoney(aux_quantity);
			}
		}
		if(aux_opt==1)
		{
			restList.add(new Money(value,quantity));
		}
	}
	
	//method for calculating the rest for client
	public int restForClient(MoneyStock firmBalanceList, List<Money> restListClient, double sum)
	{
		List<Money> firmBalance=new ArrayList<Money>();
		firmBalance=firmBalanceList.getFirmBudget();
		int opt=0;
		while(opt==0)
		{
			int i=-1;
			for(int j=0; j<firmBalance.size(); j++) //searching for highest value available for giving to client as rest
			{
				if(firmBalance.get(j).getValue()<=sum && firmBalance.get(j).getQuantityMoney()>0)
				{
					i=j;
				}
			}
			if(i==-1 && sum>0.01) // the firm cannot send the rest for client
			{
				opt=2;
			}
			else if(i==-1 && sum<0.01) //the rest was send with success
			{
				firmBalanceList.setFirmBudget(firmBalance);
				opt=1;
			}
			else 
			{
				double auxMoney=firmBalance.get(i).getValue();
				sum-=auxMoney;
				firmBalanceList.opOnMoneyList(firmBalance,i,-1);
				addRest(restListClient, auxMoney, 1);
			}
		}
		return opt;
	}
	
	public int validationInputMoney(List<Double> inputList, int index)
	{
		if (index > inputList.size()) {
			return 0;
		} else {
			return 1;
		}
	}
	
	//method for undo the list of money if the command is canceled
	public void cancelCommandMoney(MoneyStock stockListMoney, List<Money> paymentClient)
	{
		for(int i=0; i<paymentClient.size(); i++)
		{
			for(int j=0; j<stockListMoney.getFirmBudget().size();j++)
			{
				if(paymentClient.get(i).getValue()==stockListMoney.getFirmBudget().get(j).getValue())
				{
					int aux=paymentClient.get(i).getQuantityMoney()+stockListMoney.getFirmBudget().get(j).getQuantityMoney();
					stockListMoney.getFirmBudget().get(j).setQuantityMoney(aux);
				}
			}
		}
	}
}
