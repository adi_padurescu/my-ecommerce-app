package com.pink.retail.money;
/**
 * Class where we declare Money with value and quantityMoney fields
 */
public class Money {

	private double value;
	private int quantityMoney;
	
	public Money()
	{
		this.value=0;
		this.quantityMoney=0;
	}
	
	public Money(double val, int quantity)
	{
		this.value=val;
		this.quantityMoney=quantity;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getQuantityMoney() {
		return quantityMoney;
	}

	public void setQuantityMoney(int quantityMoney) {
		this.quantityMoney = quantityMoney;
	}
	
	public String toString()
	{
		return "Value: " + this.value+" lei"+" x"+this.quantityMoney;
	}
}
