package com.pink.retail.money;
/**
 * Class for reading MoneyStock and Available Money from a file 
 * Used design pattern singleton for this class
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class BLMoneyReader implements BLIMoneyReader{
	
	private static BLMoneyReader instance=null;
	private static Object object=new Object();
	
	private BLMoneyReader() {}
	public static BLMoneyReader getInstance()
	{
		if(instance==null)
		{
			synchronized(object){
				if(instance==null)
				{
					instance=new BLMoneyReader();
				}
			}
		}
		return instance;
	}

	public List<Money> readListMoneyFirm(String path) throws IOException {

		List<Money> listMoneyFirm=new ArrayList<Money>();
		FileInputStream fstream = new FileInputStream(path);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String[] parts;
		String strLine;

		// Read File Line By Line
		while ((strLine = br.readLine()) != null) {
			parts=strLine.split(",");
			double priceProduct=Double.parseDouble(parts[0]);
			int quantityProduct=Integer.parseInt(parts[1]);
			listMoneyFirm.add(new Money(priceProduct,quantityProduct));
		}
		br.close();
		return listMoneyFirm;
	}

	
	public List<Double> readListMoneyAvailable(String path) throws IOException {
		List<Double> listMoneyAvailable = new ArrayList<Double>();
		FileInputStream fstream = new FileInputStream(path);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		// Read File Line By Line
		while ((strLine = br.readLine()) != null) {
			listMoneyAvailable.add(Double.parseDouble(strLine));
		}
		br.close();
		return listMoneyAvailable;
	}

	
}
