package com.pink.retail.money;
/**
 * Class for firm money stock. 
 * Here you can access the budget of firm, the list of available bills and coins 
 * Here you find method for adding a Money Object to list or calculate the value of a budget list
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.pink.retail.util.Utility;

public class MoneyStock {
	
	private List<Money> firmBudget;
	
	public MoneyStock() throws IOException
	{
		firmBudget=new ArrayList<Money>();
		BLIMoneyReader aux=BLMoneyReader.getInstance();
		firmBudget=aux.readListMoneyFirm(Utility.LIST_OF_BUDGET_PATH);
	}
	
	public List<Double> initMoneyList() throws IOException
	{
		List<Double> initAuxList=new ArrayList<Double>();
		BLIMoneyReader aux=BLMoneyReader.getInstance();
		initAuxList=aux.readListMoneyAvailable(Utility.LIST_OF_INITIAL_MONEY_PATH);
		return initAuxList;
	}

	public List<Money> getFirmBudget() {
		return firmBudget;
	}

	public void setFirmBudget(List<Money> firmBudget) {
		this.firmBudget = firmBudget;
	}
	
	//Method for adding quantity of money to firm budget
	public void addMoneyFromList(MoneyStock firmBudgetList,int index, int quantity)
	{
		List<Money> auxList=firmBudgetList.getFirmBudget();
		int aux; 
		aux=auxList.get(index-1).getQuantityMoney()+quantity;
		auxList.get(index-1).setQuantityMoney(aux);
		firmBudgetList.setFirmBudget(auxList);
	}
	
	//Method for adding a quantity of money to a list of money 
	public void opOnMoneyList(List<Money> moneyList, int index, int quantity)
	{
		int aux=moneyList.get(index).getQuantityMoney()+quantity;
		moneyList.get(index).setQuantityMoney(aux);
	}
	
	//Method for calculating firm balance from budget
	public double initialBalanceFirm(MoneyStock moneyList)
	{
		double firm_sum=0;
		for(int i=0; i<moneyList.getFirmBudget().size(); i++)
		{
			firm_sum+=moneyList.getFirmBudget().get(i).getValue()*moneyList.getFirmBudget().get(i).getQuantityMoney();
		}
		return firm_sum;
	}
	
	//Adding list of client payment to firm money list
	public void updateFirmBalance(MoneyStock moneyList, List<Money> clientPayment)
	{
		List<Money> aux_list=new ArrayList<Money>();
		aux_list=moneyList.getFirmBudget();
		for(int i=0; i<clientPayment.size(); i++)
		{
			for(int j=0; j<aux_list.size(); j++)
			{
				if(clientPayment.get(i).getValue()==aux_list.get(j).getValue())
				{
					int quantity_aux=clientPayment.get(i).getQuantityMoney()+aux_list.get(j).getQuantityMoney();
					aux_list.get(j).setQuantityMoney(quantity_aux);
					
				}
			}
		}
		moneyList.setFirmBudget(aux_list);
	}
}
