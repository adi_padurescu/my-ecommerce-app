package com.pink.retail.money;
/**
 * Interface with methods for reading money stock from available money file 
 */
import java.io.IOException;
import java.util.List;

public interface BLIMoneyReader {

	public List<Money> readListMoneyFirm(String path) throws IOException;
	
	public List<Double> readListMoneyAvailable(String path) throws IOException;
	
}
