package com.pink.retail.customer;

import java.util.List;

public interface BLICustomerDAO {
	
	public void insertCustomer(CustomerEntity customer);
	public void updateCustomerByCustomerId(CustomerEntity customer);
	public CustomerEntity findCustomerByCustomerId(int id);
	public CustomerEntity findCustomerByEmailAndPassword(String email,String password );
	public CustomerEntity findCustomerByEmail(String email );
	public List<CustomerEntity> findAllCustomers();
	public void deleteCustomerByCustomerId( int id);
	public void deleteAllCustomers();
	
}
