package com.pink.retail.customer;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/customer")
public class CustomerService {
	
	private BLICustomerDAO customerDAO;
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CustomerEntity> getCustomers() {
		customerDAO = new BLCustomerDAO();
		List<CustomerEntity> customers= new ArrayList<CustomerEntity>();
		customers= customerDAO.findAllCustomers();
		return customers;
	}
	
	@GET
	@Path("/{customerID}")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerEntity getCustomerByID(@PathParam("customerID") int customerID) {
		customerDAO = new BLCustomerDAO();
		return customerDAO.findCustomerByCustomerId(customerID);
	}
	
	@POST
	@Path("/loginCustomer")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerEntity getCustomerByEmailAndPassword(CustomerEntity customer) {
		customerDAO = new BLCustomerDAO();
		return customerDAO.findCustomerByEmailAndPassword(customer.getEmail(), customer.getPassword());
	}
	
	@POST
	@Path("/getEmail")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerEntity getCustomerByEmail(CustomerEntity customer) {
		customerDAO = new BLCustomerDAO();
		return customerDAO.findCustomerByEmail(customer.getEmail());
	}
	
	 @PUT
	 @Path("/addCustomer")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void insertCustomer(CustomerEntity customer) {
		 customerDAO = new BLCustomerDAO();
	     customerDAO.insertCustomer(customer);
	  }
	
	 @POST
	 @Path("/updateCustomer/{customerID}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void updateCustomer(CustomerEntity customer,@PathParam("customerID") int customerID) {
		 customerDAO = new BLCustomerDAO();
		 customer.setCustomerId(customerID);
	     customerDAO.updateCustomerByCustomerId(customer);
	  }
	 
	 @DELETE
	 @Path("/deleteCustomer/{customerID}")
	 @Produces(MediaType.APPLICATION_JSON)
	 public void deleteCustomer(@PathParam("customerID") int customerID) {
		 customerDAO = new BLCustomerDAO();
		 customerDAO.deleteCustomerByCustomerId(customerID);
	 }
	
}
