package com.pink.retail.customer;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Root;
import com.pink.retail.display.DisplayData;
import com.pink.retail.util.Utility;

public class BLCustomerDAO implements BLICustomerDAO {
	DisplayData display = new DisplayData();
	EntityManager entityManager;
	EntityManagerFactory entityManagerFactory;
	
	public BLCustomerDAO() {
		entityManagerFactory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = entityManagerFactory.createEntityManager();
	}
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public void insertCustomer( CustomerEntity customer) {
		display.message(Utility.CUSTOMER_INSERT_NEW);
		display.showCustomer(customer);
		entityManager.getTransaction().begin();
		entityManager.persist(customer);
		entityManager.getTransaction().commit();
	}

	public void updateCustomerByCustomerId(CustomerEntity customer) {
		entityManager.getTransaction().begin();
		if (entityManager.find(CustomerEntity.class,customer.getCustomerId()) == null) {
			display.message(Utility.NO_CUSTOMER);
		} else {
			display.message(Utility.CUSTOMER_UPDATE_BY_ID + customer.getCustomerId() + Utility.CUSTOMER_NEW_CUSTOMER_UPDATE);
			display.showCustomer(customer);
			entityManager.merge(customer);
		}
		entityManager.getTransaction().commit();
	}

	@Override
	public CustomerEntity findCustomerByCustomerId(int id) {	
		display.message(Utility.CUSTOMER_FIND_BY_ID + id);
		entityManager.getTransaction().begin();
		CustomerEntity customer = entityManager.find(CustomerEntity.class, id);
		entityManager.getTransaction().commit();
		return customer;
	}

	public CustomerEntity findCustomerByEmailAndPassword(String email,String password ) {
		display.message(Utility.CUSTOMER_FIND_BY_EMAIL_AND_PASSWORD + email+ " "+password );
		entityManager.getTransaction().begin();
		List<CustomerEntity> listOfCustomers=entityManager.createQuery("SELECT c FROM CustomerEntity c where c.email='"+email+"' and c.password='"+password+"'",CustomerEntity.class)
				.getResultList();
		entityManager.getTransaction().commit();
		if(listOfCustomers.size()!=0){
			return listOfCustomers.get(0);
		}else {
			return null;
		}
	}
	
	public CustomerEntity findCustomerByEmail(String email ) {
		entityManager.getTransaction().begin();
		List<CustomerEntity> listOfCustomers=entityManager.createQuery("SELECT c FROM CustomerEntity c where c.email='"+email+"'",CustomerEntity.class)
				.getResultList();
		entityManager.getTransaction().commit();
		if(listOfCustomers.size()!=0){
			return listOfCustomers.get(0);
		}else {
			return null;
		}
	}
	
	@Override
	public List<CustomerEntity> findAllCustomers() {
		display.message(Utility.CUSTOMER_FIND_ALL);
		EntityTransaction findAddressesTransaction = entityManager.getTransaction();
		findAddressesTransaction.begin();
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<CustomerEntity> criteria = builder.createQuery(CustomerEntity.class);
		Root<CustomerEntity> customer = criteria.from(CustomerEntity.class);
		criteria.select(customer);

		List<CustomerEntity> customersList = entityManager.createQuery(criteria).getResultList();
		findAddressesTransaction.commit();
		return customersList;

	}

	@Override
	public void deleteCustomerByCustomerId(int id) {
		entityManager.getTransaction().begin();
		if (entityManager.find(CustomerEntity.class, id) == null) {
			display.message(Utility.NO_CUSTOMER);
		} else {
			display.message(Utility.CUSTOMER_DELETE_BY_ID + id);
			CustomerEntity customer = entityManager.getReference(CustomerEntity.class, id);
			entityManager.remove(customer);
		}
		entityManager.getTransaction().commit();
	}

	@Override
	public void deleteAllCustomers() {	
		entityManager.getTransaction().begin();
		Query query = entityManager.createNativeQuery("DELETE FROM Customer");
		if(query.executeUpdate()!=0) {
			display.message(Utility.CUSTOMER_DELETE_ALL);
		}else {
			display.message(Utility.NO_CUSTOMERS);
		}
		entityManager.getTransaction().commit();
	}

}
