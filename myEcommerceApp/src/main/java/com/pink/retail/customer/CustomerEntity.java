package com.pink.retail.customer;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.pink.retail.address.AddressEntity;
import com.pink.retail.order.OrderEntity;

@XmlRootElement
@Entity
@Table(name = "Customer")
public class CustomerEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CUSTOMER_ID", unique = true, nullable = false)
	private int customerId;

	@Column(name = "FIRST_NAME", nullable = false, length = 50)
	private String firstName;

	@Column(name = "LAST_NAME", nullable = false, length = 50)
	private String lastName;

	@Column(name = "EMAIL", unique = true, nullable = false, length = 50)
	private String email;

	@Column(name = "PHONE_NUMBER", nullable = false, length = 50)
	private String phoneNumber;
	
	@Column(name = "PASSWORD", nullable = false, length = 50)
	private String password;

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "costumers")
	private List<AddressEntity> addresses;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.PERSIST,targetEntity=OrderEntity.class,fetch=FetchType.EAGER)
	private List<OrderEntity> listOrders;

	public List<OrderEntity> getListOrders() {
		return listOrders;
	}

	public void setListOrders(List<OrderEntity> listOrders) {
		this.listOrders = listOrders;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<AddressEntity> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressEntity> addresses) {
		this.addresses = addresses;
	}

	public CustomerEntity() {
		super();
	}

	public CustomerEntity(String firstName, String lastName, String email, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	
	public CustomerEntity(int customerId, String firstName, String lastName, String email, String phoneNumber) {
		super();
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public String toString() {
		return "\n Customer id: " + this.customerId + "\n First name: " + this.firstName + "\n Last name: " + this.lastName
				+ "\n Email: " + this.email + "\nPhone number: " + this.phoneNumber;
	}

}
