package com.pink.retail.display;
/**
 * Class for showing messages on Console
 */
import java.util.List;

import org.apache.log4j.Logger;

import com.pink.retail.address.AddressEntity;
import com.pink.retail.customer.CustomerEntity;
import com.pink.retail.money.Money;
import com.pink.retail.order.OrderEntity;
import com.pink.retail.product.Product;
import com.pink.retail.product.ProductEntity;
import com.pink.retail.product.ProductStock;


public class DisplayData {

	public static final Logger LOGGER=Logger.getLogger(DisplayData.class);
	
	//method for showing the list of products
	public void showProductList(List<Product> productList)
	{
		System.out.println();
		for(int i=0; i<productList.size(); i++)
		{
			LOGGER.info(i+1+".");
			LOGGER.info(productList.get(i).toString());
		}
	}
	
	//method for showing the list of shopping cart
	public void showCartList(List<Integer> index_buy,List<Integer> quantity_buy, ProductStock firm_list)
	{
		for(int i=0; i<index_buy.size(); i++)
		{
			System.out.println(i+1+"."+firm_list.getListProduct().get(index_buy.get(i)-1).getName()+" x"+quantity_buy.get(i)+" with price/item: "+firm_list.getListProduct().get(index_buy.get(i)-1).getPrice()+" lei");
		}
	}
	
	//method for showing the list of money 
	public void showMoneyList(List<Money> money_list)
	{
		for(int i=0; i<money_list.size(); i++)
		{
			System.out.print(i+1+".");
			System.out.println(money_list.get(i));
		}
	}
	
	public void message(String msg)
	{
		LOGGER.info(msg);

	}

	public void showDoubleList(List<Double> listMoney)
	{
		for(int i=0; i<listMoney.size();i++)
		{
			System.out.println(i+1+". "+"Value : "+listMoney.get(i)+" lei");
		}
	}
	
	public void showProduct(ProductEntity product)
	{
		LOGGER.info(product.toString());
	}
	
	public void showCustomer(CustomerEntity customer) 
	{
		LOGGER.info(customer.toString());
	}
	
	public void showAddress(AddressEntity addressEntity) 
	{
		LOGGER.info(addressEntity);
	}
	
	public void showOrders(OrderEntity order)
	{
		LOGGER.info(order.toString());
	}
}
