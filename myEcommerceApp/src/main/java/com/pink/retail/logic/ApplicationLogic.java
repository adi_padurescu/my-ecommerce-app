package com.pink.retail.logic;
/**
 * Class that has all business logic methods
 * First the client select the products, then he need to pay for them, if he pay more than command price, he receive rest.
 * If the firm does not has bill to pay the rest, the command is automatic canceled.
 * If the quantity product is 0, the Application will show "Sold out"
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.pink.retail.display.DisplayData;
import com.pink.retail.money.Balance;
import com.pink.retail.money.Currency;
import com.pink.retail.money.Money;
import com.pink.retail.money.MoneyStock;
import com.pink.retail.product.ProductStock;
import com.pink.retail.product.ShoppingCart;
import com.pink.retail.util.Utility;

public class ApplicationLogic {

	//part of application where client select the products
	public double clientSessionProduct(List<Integer> buyyer_index,List<Integer> buyyer_quantity,ProductStock firm_stock, Scanner inRead,DisplayData display_data_messages) throws IOException {
		int index_buy = 0, quantity_buy = 0; 
		int opt=1;
		ShoppingCart verfic_shopping_cart=new ShoppingCart();
		ProductStock list_of_products=new ProductStock();
		double price=0;
			while (opt==1) {
				display_data_messages.message(Utility.BUYING_FIRST_MESSAGE);
				display_data_messages.showProductList(firm_stock.getListProduct());
				display_data_messages.message(Utility.BUYING_SELECT_INDEX_PRODUCT);
				index_buy = inRead.nextInt();
				int verification_var=verfic_shopping_cart.validationInputValues(index_buy,firm_stock);
				while(verification_var==0)
				{
					display_data_messages.message(Utility.BUYING_WRONG_INDEX_PRODUCT);
					display_data_messages.message(Utility.BUYING_SELECT_INDEX_PRODUCT);
					index_buy = inRead.nextInt();
					verification_var=verfic_shopping_cart.validationInputValues(index_buy,firm_stock);
				}
				display_data_messages.message(Utility.BUYING_SELECT_QUANTITY_PRODUCT);
				quantity_buy = inRead.nextInt();
				int verification_var_quantity=verfic_shopping_cart.validationInputQuantity(quantity_buy, index_buy, firm_stock);
				while(verification_var_quantity==0)
				{
					display_data_messages.message(Utility.BUYING_WRONG_QUANTITY_PRODUCT);
					display_data_messages.message(Utility.BUYING_SELECT_QUANTITY_PRODUCT);
					quantity_buy = inRead.nextInt();
					verification_var_quantity=verfic_shopping_cart.validationInputQuantity(quantity_buy, index_buy, firm_stock);
				}
				verfic_shopping_cart.addItemToCart(buyyer_index, buyyer_quantity, index_buy, quantity_buy);
				list_of_products.removeItemFromProductList(firm_stock, index_buy, quantity_buy);
				price=verfic_shopping_cart.currentePrice(price, index_buy, quantity_buy, firm_stock);
				opt=firm_stock.checkForClosingShop(firm_stock);
				if(opt==1)
				{
					display_data_messages.message(Utility.BUYING_CONFIRMATION_MESSAGE);
					opt = inRead.nextInt();	
				}
			}
			return price;
	}
	
	//part of application where client pay the products
	public double clientPayment(double price,MoneyStock firmBudgetList, List<Money> restClientList, double balanceFirm,Scanner inRead,List<Integer> index_buyer, List<Integer> quantity_buyer, ProductStock firm_stock,DisplayData display_data_messages) throws IOException
	{ 
		double sum=price;
		double currentPrice=0;
		double rest=0;
		List<Double> available_value_money=new ArrayList<Double>();
		available_value_money=firmBudgetList.initMoneyList();
		List<Money> list_payment_client=new ArrayList<Money>();
		Balance validation_input_index=new Balance();
		int opt=1,opt2=-1;
		Balance balance_rest =new Balance();
		int index_money=0, quantity_money=0;
		ApplicationLogic rest_for_client=new ApplicationLogic();
		while(opt==1) //while for entering the money till the client pay all the price
		{
			display_data_messages.showDoubleList(available_value_money);
			display_data_messages.message(Utility.PAYMENT_NEED_PAY);
			display_data_messages.message(sum+" lei");
			display_data_messages.message(Utility.PAYMENT_SELECT_MONEY);
			index_money=inRead.nextInt();
			int validOpt=validation_input_index.validationInputMoney(available_value_money,index_money);
			while(validOpt==0)
			{
				display_data_messages.message(Utility.PAYMENT_WRONG_INDEX);
				display_data_messages.message(Utility.PAYMENT_SELECT_MONEY);
				index_money=inRead.nextInt();
				validOpt=validation_input_index.validationInputMoney(available_value_money,index_money);
			}
			display_data_messages.message(Utility.PAYMENT_QUANTITY_MONEY);
			quantity_money = inRead.nextInt();
			currentPrice=firmBudgetList.getFirmBudget().get(index_money-1).getValue()*quantity_money;
			firmBudgetList.addMoneyFromList(firmBudgetList, index_money, quantity_money); //update of firm budged with money that client paid
			balance_rest.addRest(list_payment_client, firmBudgetList.getFirmBudget().get(index_money-1).getValue(), quantity_money); //update client list pay with value of the money
			sum-=currentPrice;
			sum=Math.round(sum * 100.0) / 100.0; //round by two decimals
			if(sum<=0) //outing from payment method, the client paid the value of product
			{
				opt=2;
			}
		}
		balanceFirm=rest_for_client.paymentRestClient(display_data_messages,sum,balanceFirm,price,index_buyer, quantity_buyer,firm_stock,rest, opt2,list_payment_client,firmBudgetList,restClientList);
		balanceFirm=Math.round(balanceFirm * 100.0) / 100.0;
		return balanceFirm;
	}
	
	//part of application where we calculate the rest for client
	public double paymentRestClient(DisplayData display_data_messages, double sum, double balanceFirm, double price,List<Integer> index_buyer, List<Integer> quantity_buyer, ProductStock firm_stock,double rest, int opt2,	List<Money> list_payment_client,MoneyStock firmBudgetList, List<Money> restClientList)
	{
		Balance balance_rest=new Balance();
		ShoppingCart shopping_cart_instance=new ShoppingCart();
		Balance cancel_money=new Balance();
		if(sum==0) //The client does not need rest
		{
			display_data_messages.message(Utility.REST_SUCCESS_PAY);
			balanceFirm+=price;
			display_data_messages.message(Utility.REST_BILL_INFO);
			display_data_messages.message(Utility.REST_PRODUCT_BOUGHT);
			display_data_messages.showCartList(index_buyer, quantity_buyer, firm_stock);
			display_data_messages.message(Utility.REST_CLIENT_PAYMENT);
			display_data_messages.showMoneyList(list_payment_client);
		}
		
		else //Calculating the rest for client
		{
			rest=(-1)*sum;
			opt2=balance_rest.restForClient(firmBudgetList, restClientList, rest);
			if(opt2==1) //The rest was send with success
			{
				display_data_messages.message(Utility.REST_SUCCESS_PAY);
				balanceFirm+=price;
				display_data_messages.message(Utility.REST_BILL_INFO);
				display_data_messages.message(Utility.REST_PRODUCT_BOUGHT);
				display_data_messages.showCartList(index_buyer, quantity_buyer, firm_stock);
				display_data_messages.message(Utility.REST_CLIENT_PAYMENT);
				display_data_messages.showMoneyList(list_payment_client);
				display_data_messages.message(Utility.REST_CLIENT_REST);
				display_data_messages.showMoneyList(restClientList);
				display_data_messages.message(Utility.REST_FIRM_BUDGET);
				display_data_messages.showMoneyList(firmBudgetList.getFirmBudget());
				
			}
			else //The rest can not be send, so the comand was canceled
			{
				display_data_messages.message(Utility.REST_CANCEL_PAY);
				shopping_cart_instance.commandCancel(index_buyer, quantity_buyer, firm_stock);
				cancel_money.cancelCommandMoney(firmBudgetList, list_payment_client);
			}
		}
		return balanceFirm;
	}
	
	//part of application where the client choose if he wants to pay with EUR or USD
	public double paymentForeignValue(double exchange_val, double price, Scanner inRead,List<Money> paymentForClient, double balance_firm,MoneyStock firm_money, List<Money> restClientList,List<Integer> index_buyer, List<Integer> quantity_buyer, ProductStock firm_stock, DisplayData display_data_messages) throws IOException
	{
		double summary=price;
		int opt=1,opt2=-1;
		double currentPriceInput=0,rest=0;
		double currentPriceChange=0;
		Balance balance_rest_instance=new Balance();
		ShoppingCart shopping_remove_item=new ShoppingCart();
		Balance cancel_money=new Balance();
		Currency divide_in_bills=new Currency();
		while(opt==1)
		{
			display_data_messages.message(Utility.PAYMENT_NEED_PAY);
			display_data_messages.message(summary+" lei");
			display_data_messages.message(Utility.FOREIGN_VALUE_PAY);
			currentPriceInput=inRead.nextDouble();
			currentPriceChange=exchange_val*currentPriceInput;
			currentPriceChange=Math.round(currentPriceChange*100.0)/100.0;
			divide_in_bills.divideNumberInMoney(currentPriceChange, paymentForClient); //The value of command is converted in bills
			summary-=currentPriceChange;
			summary=Math.round(summary*100.0)/100.0;
			if(summary<=0)
			{
				opt=2;
				firm_money.updateFirmBalance(firm_money, paymentForClient); //Update budget firm with the bills and coins that client has payed
			}	
		}	
		if(summary==0) //The client doesn't need rest
		{
			display_data_messages.message(Utility.REST_SUCCESS_PAY);
			balance_firm+=price;
			display_data_messages.message(Utility.REST_BILL_INFO);
			display_data_messages.message(Utility.REST_PRODUCT_BOUGHT);
			display_data_messages.showCartList(index_buyer, quantity_buyer, firm_stock);
			display_data_messages.message(Utility.REST_CLIENT_PAYMENT);
			display_data_messages.showMoneyList(paymentForClient);
		}
		else
		{
			rest=(-1)*summary;
			opt2=balance_rest_instance.restForClient(firm_money, restClientList, rest);
			if(opt2==1) //The rest was send with success
			{
				display_data_messages.message(Utility.REST_SUCCESS_PAY);
				display_data_messages.message(Utility.REST_BILL_INFO);
				balance_firm+=price;
				display_data_messages.message(Utility.REST_PRODUCT_BOUGHT);
				display_data_messages.showCartList(index_buyer, quantity_buyer, firm_stock);
				display_data_messages.message(Utility.REST_CLIENT_PAYMENT);
				display_data_messages.showMoneyList(paymentForClient);
				display_data_messages.message(Utility.REST_CLIENT_REST);
				display_data_messages.showMoneyList(restClientList);
				display_data_messages.message(Utility.REST_FIRM_BUDGET);
				display_data_messages.showMoneyList(firm_money.getFirmBudget());
				
			}
			else //The rest can not be send, so the command was canceled
			{
				display_data_messages.message(Utility.REST_CANCEL_PAY);
				shopping_remove_item.commandCancel(index_buyer, quantity_buyer, firm_stock); //Refacerea listei cu produsele firmei
				cancel_money.cancelCommandMoney(firm_money, paymentForClient); //Refacerea listei cu bugetul firmei
			}
		}
		balance_firm=Math.round(balance_firm * 100.0) / 100.0;
		return balance_firm;
	}
	public void clientCurrentSession() throws IOException 
	{
		ProductStock firm_stock=new ProductStock(); 
		MoneyStock firm_money=new MoneyStock();
		Scanner inRead=new Scanner(System.in);
		int closingShop=1; 
		double price=0;
		double balance_firm=firm_money.initialBalanceFirm(firm_money);
		Currency choosing_currence_value=new Currency();
		DisplayData display_data_messages=new DisplayData();
		ApplicationLogic session_buying_products=new ApplicationLogic();
		try {
			display_data_messages.message(Utility.CLIENT_BALANCE_FIRM);
			display_data_messages.message(balance_firm+" lei");
			display_data_messages.message("\n");
			while (closingShop==1) {
				List<Integer> buyyer_index=new ArrayList<Integer>(); //the index which was choose by client
				List<Integer> buyyer_quantity=new ArrayList<Integer>(); //the quantity which was choose by client
				price = session_buying_products.clientSessionProduct(buyyer_index, buyyer_quantity, firm_stock, inRead,display_data_messages); //returneaza pretul comenzii facuta de client
				display_data_messages.message(Utility.CLIENT_PRODUCT_LIST_SELECTED);
				display_data_messages.showCartList(buyyer_index, buyyer_quantity, firm_stock);
				display_data_messages.message(Utility.CLIENT_NEED_TO_PAY);
				display_data_messages.message(price+" lei");
				display_data_messages.message(Utility.CLIENT_SELECT_CURRENCE);
				int opt=inRead.nextInt();
				if(opt==1)
				{
					List<Money> restForClient = new ArrayList<Money>();
					balance_firm = session_buying_products.clientPayment(price, firm_money, restForClient, balance_firm, inRead, buyyer_index,buyyer_quantity, firm_stock,display_data_messages); // plata si restul
					display_data_messages.message(Utility.CLIENT_FIRM_BUDGET);
					display_data_messages.message(balance_firm+" lei");
					display_data_messages.message("\n");
				}
				else
				{
					display_data_messages.message(Utility.CLIENT_CHOOSE_NEW_VALUE);
					opt=inRead.nextInt();
					double exchange_val=choosing_currence_value.chooseCurrence(opt);
					List<Money> paymentClient = new ArrayList<Money>();
					List<Money> restClientList = new ArrayList<Money>();
					balance_firm=session_buying_products.paymentForeignValue(exchange_val, price, inRead, paymentClient, balance_firm, firm_money, restClientList, buyyer_index, buyyer_quantity, firm_stock,display_data_messages);
					display_data_messages.message(Utility.CLIENT_FIRM_BUDGET);
					display_data_messages.message(balance_firm+" lei");
					display_data_messages.message("\n");
				}
				closingShop=firm_stock.checkForClosingShop(firm_stock);
			}
		}
		catch (InputMismatchException e) {
			System.out.println(Utility.BAD_INFO_INPUT_ERROR);
		} finally {
			inRead.close();
		}
	}
	
}
