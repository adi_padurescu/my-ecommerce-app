package com.pink.retail.runner;
/**
 * The class where we run our Ecommerce Application
 */

import java.io.IOException;

import com.pink.retail.logic.ApplicationLogic;

public class ApplicationRunner {

	 public static void main( String[] args ) throws IOException 
	    {
		 	ApplicationLogic runner_application_session=new ApplicationLogic();
		 	runner_application_session.clientCurrentSession();
	    }
}
