package com.pink.retail.runner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.BasicConfigurator;

import com.pink.retail.address.AddressEntity;
import com.pink.retail.address.BLAddressDAO;
import com.pink.retail.customer.BLCustomerDAO;
import com.pink.retail.customer.CustomerEntity;
import com.pink.retail.display.DisplayData;
import com.pink.retail.entities.JPAUtil;
import com.pink.retail.order.BLOrderDAO;
import com.pink.retail.order.OrderEntity;
import com.pink.retail.product.BLProductDAO;
import com.pink.retail.product.ProductEntity;
import com.pink.retail.util.Utility;

public class RunnerDAO {

	public static void main(String[] args) throws IOException {
		BasicConfigurator.configure();
		DisplayData display = new DisplayData();

		// begin
		JPAUtil factoryEntity=new JPAUtil();
		factoryEntity.setFactory(factoryEntity.getEntityManagerFactory());
		EntityManager entityManager = factoryEntity.getFactory().createEntityManager();
		factoryEntity.readMySqlFromFile(Utility.MYSQL_INSERT_ADDRESS,entityManager);
		factoryEntity.readMySqlFromFile(Utility.MYSQL_INSERT_CUSTOMER,entityManager);
		factoryEntity.readMySqlFromFile(Utility.MYSQL_INSERT_PRODUCT,entityManager);
		// OPERATIONS FOR ADDRESS TABLE
		BLAddressDAO addressDAO = new BLAddressDAO();
		AddressEntity newAddress = new AddressEntity();
		addressDAO.setEntityManager(entityManager);
		newAddress.setStreetAddress("Gheroghe Asachi");
		newAddress.setCityAddress("Iasi");
		newAddress.setCountyAddress("Iasi");
		newAddress.setPostalCode("091132A");

		addressDAO.insertAddress(newAddress);

		AddressEntity auxAddress = addressDAO.findAddressByID(1);
		display.showAddress(auxAddress);

		AddressEntity updateAddress = new AddressEntity();
		updateAddress.setAddressId(2);
		updateAddress.setCityAddress("Focsani");
		updateAddress.setCountyAddress("Vrancea");
		updateAddress.setPostalCode("091132A");
		updateAddress.setStreetAddress("1 Decembrie");
		addressDAO.updateAddress(updateAddress);

		List<AddressEntity> listOfAddress = addressDAO.findAllAddress();
		for (int i = 0; i < listOfAddress.size(); i++) {
			display.showAddress(listOfAddress.get(i));
		}
		// addressDAO.deleteAllAddress(entityManager);

		List<CustomerEntity> listOfCustomerAddress = new ArrayList<>();
		listOfCustomerAddress.add(new CustomerEntity("Alex", "Test", "test@yahoo.com", "0722332233"));
		listOfCustomerAddress.add(new CustomerEntity("Alin", "Test", "test2@yahoo.com", "0722332244"));
		newAddress.setCostumer(listOfCustomerAddress);
		newAddress.setCityAddress("Dorohoi");
		newAddress.setStreetAddress("Test");
		addressDAO.insertAddress(newAddress);


		// OPERATIONS FOR CUSTOMER TABLE
		BLCustomerDAO customerDAO = new BLCustomerDAO();
		customerDAO.setEntityManager(entityManager);
		customerDAO.setEntityManagerFactory(factoryEntity.getFactory());
		CustomerEntity customer1 = new CustomerEntity("Mara", "Ionescu", "mara@yahoo.com", "073456");
		CustomerEntity customer2 = new CustomerEntity("Oana", "Monescu", "oana@yahoo.com", "077856");
		CustomerEntity customerUpdate = new CustomerEntity(1, "Mariana", "Ionescu", "mariana@yahoo.com", "073456");

		customerDAO.insertCustomer(customer1);
		customerDAO.insertCustomer(customer2);

		customerDAO.updateCustomerByCustomerId(customerUpdate);

		int id=1;
		CustomerEntity customer = customerDAO.findCustomerByCustomerId(id);
		if(customer!=null) {
			display.showCustomer(customer);
		}else {
			display.message(Utility.NO_CUSTOMER+id);
		}

		List<CustomerEntity> customersList = customerDAO.findAllCustomers();
		if(customersList!=null) {
			for (CustomerEntity c : customersList) {
				display.showCustomer(c);
			}
		}else {
			display.message(Utility.NO_CUSTOMERS);
		}


		//customerDAO.deleteCustomerByCustomerId(entityManager, 2);
		//customerDAO.deleteAllCustomers(entityManager);

		
		// OPERATIONS FOR ORDERS TABLE
		BLOrderDAO orderDAO = new BLOrderDAO();
		orderDAO.setEntityManager(entityManager);
		OrderEntity order1 = new OrderEntity("20.05.2018", "Pending", "LEI");
		OrderEntity order2 = new OrderEntity("19.05.2018","Shipped","LEI");
		OrderEntity updateOrder = new OrderEntity(1,"20.05.2018", "Shipped", "LEI");

		orderDAO.insertOrders(order1);
		orderDAO.insertOrders(order2);

		orderDAO.updateOrders(updateOrder);

		OrderEntity order = orderDAO.findOrdersByID(1);
		display.showOrders(order);
		List<OrderEntity> ordersList = orderDAO.findAllOrders();
		if(ordersList != null) {
			for(int i = 0; i < ordersList.size(); i++) {
				display.showOrders(ordersList.get(i));
			}
		} else {
			display.message(Utility.NO_ORDERS);
		}

		//orderDAO.deleteByIdOrders(entityManager, 1);
		//orderDAO.deleteAllOrders(entityManager);

		// OPERATIONS FOR PRODUCT TABLE
		BLProductDAO productDAO = new BLProductDAO();
		//productDAO.setEntityManager(entityManager);
		ProductEntity product = new ProductEntity("Laptop i7", "Asus", 25, 3250);
		productDAO.insertProduct(product);

		product = new ProductEntity("Mouse", "Microsoft", 25, 35);
		product.setProductID(2);
		productDAO.updateProduct(product);

		productDAO.deleteProductByID(1);
		//display.showProduct(productDAO.getProductByID(5, entityManager));

		List<ProductEntity> products = productDAO.getAllProducts();
		for (ProductEntity p : products) {
			display.showProduct(p);
		}
		//productDAO.deleteAllProducts(entityManager);

		List<AddressEntity> listOfAddressCustomer = new ArrayList<AddressEntity>();
		listOfAddressCustomer.add(new AddressEntity("Test", "Test", "Bucuresti", "Bucuresti"));
		listOfAddressCustomer.add(new AddressEntity("Tudor Vladimirescu", "Test", "Iasi", "Iasi"));
		customer1.setAddresses(listOfAddressCustomer);

		ordersList.add(new OrderEntity("05-05-2005","test","test"));
		customer2.setListOrders(ordersList);
		customerDAO.insertCustomer(customer2);

		List<OrderEntity> listOfOrdersProduct=new ArrayList<OrderEntity>();
		listOfOrdersProduct.add(new OrderEntity("22.10.2018","delivered","lei"));
		listOfOrdersProduct.add(new OrderEntity("24.10.2018","delivered","lei"));
		product=new ProductEntity("test","test",20,20);
		product.setOrders(listOfOrdersProduct);
		productDAO.insertProduct(product);

		List<ProductEntity> listOfProductOrders =new ArrayList<ProductEntity>();
		listOfProductOrders.add(new ProductEntity("LG","LG",520,520));
		listOfProductOrders.add(new ProductEntity("LG1","LG1",5250,5250));
		OrderEntity p=new OrderEntity("06.06.2006","CLOSE","LEI");
		p.setListProduct(listOfProductOrders);
		orderDAO.insertOrders(p);


	
		entityManager.close();
		//factory.close();
		factoryEntity.shutdown();
	}
}
