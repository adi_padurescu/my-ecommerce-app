package com.pink.retail.product;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.pink.retail.product.BLIProductReader;
import com.pink.retail.product.BLProductReader;
import com.pink.retail.product.Product;

public class BLProductReaderTest {

	@Test
	public void readListProductTest() throws IOException
	{
		List<Product> testList=new ArrayList<Product>();
		BLIProductReader reader=new BLProductReader();
		testList=reader.readListProduct("src/main/resources/products.txt");
		assertTrue(testList.size()==10);
	}
}
