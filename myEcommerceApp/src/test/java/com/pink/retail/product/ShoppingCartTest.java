package com.pink.retail.product;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.product.ProductStock;
import com.pink.retail.product.ShoppingCart;

public class ShoppingCartTest {
	
	ShoppingCart shoppingCartTest;
	List<Integer> indexBuyerTest;
	List<Integer> quantityBuyerTest;
	ProductStock listProductTest;
	
	@Before
	public void ShoppingCart() throws IOException
	{
		shoppingCartTest=new ShoppingCart();
		indexBuyerTest=new ArrayList<Integer>();
		quantityBuyerTest=new ArrayList<Integer>();
		listProductTest=new ProductStock();
	}
	
	@Test
	public void currentePriceTest()
	{
		double price=0;
		price=shoppingCartTest.currentePrice(price, 1, 1, listProductTest);
		assertTrue(price==2420.99);
	}

	@Test
	public void validationInputValueTest()
	{
		assertTrue(shoppingCartTest.validationInputValues(30, listProductTest)==0);
	}
	
	@Test
	public void validationInputQuantityTest()
	{
		assertTrue(shoppingCartTest.validationInputQuantity(1000, 2, listProductTest)==0);
	}
	
	@Test
	public void commandCancelTest()
	{
		indexBuyerTest.add(1);
		quantityBuyerTest.add(10);
		shoppingCartTest.commandCancel(indexBuyerTest, quantityBuyerTest, listProductTest);
		assertTrue(listProductTest.getListProduct().get(0).getQuantity()==60);
		
	}
}
