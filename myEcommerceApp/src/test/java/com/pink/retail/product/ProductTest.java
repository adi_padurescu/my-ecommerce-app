package com.pink.retail.product;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.product.Product;

public class ProductTest {

	public Product testProduct;
	
	@Before
	public void Product()
	{
		testProduct=new Product("test",10,10.5);
	}
	
	@Test
	public void getNameTest()
	{
	
		assertTrue("Testing get method for Name",testProduct.getName()=="test");
	}
	
	@Test
	public void setNameTest()
	{
		testProduct.setName("Itest");
		assertTrue(testProduct.getName()=="Itest");
	}
	
	@Test
	public void getQuantityTest()
	{
	
		assertTrue(testProduct.getQuantity()==10);
	}
	
	@Test
	public void setQuantityTest()
	{
		testProduct.setQuantity(20);
		assertTrue(testProduct.getQuantity()==20);
	}
	
	@Test
	public void getPriceTest()
	{
		assertTrue(testProduct.getPrice()==10.5);
	}
	@Test
	public void setPriceTest()
	{
		testProduct.setPrice(20.5);
		assertTrue(testProduct.getPrice()==20.5);
	}
	
	
}
