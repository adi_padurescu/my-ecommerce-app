package com.pink.retail.product;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.product.ProductStock;

public class ProductStockTest {

	public ProductStock testListProductStock;
	
	@Before
	public void ProductStock() throws IOException
	{
		testListProductStock=new ProductStock();
	}
	
	@Test
	public void getListProductTest()
	{
		assertTrue(testListProductStock.getListProduct().size()==10);
	}
	
	@Test
	public void removeItemFromProductListJustQuantityTest()
	{
		assertTrue(testListProductStock.removeItemFromProductList(testListProductStock, 1, 1)==1);
	}
	
	@Test
	public void removeItemFromProductListTooMuchQuantityTest()
	{
		assertTrue(testListProductStock.removeItemFromProductList(testListProductStock, 9, 150)==0);
	}
}
