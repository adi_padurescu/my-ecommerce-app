package com.pink.retail.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pink.retail.customer.BLCustomerDAO;
import com.pink.retail.customer.CustomerEntity;

public class BLCustomerDAOTest {
	BLCustomerDAO blCustomerDAO;
	EntityManagerFactory factory;
	EntityManager entityManager;
	@Before
	public void before() {
		blCustomerDAO = new BLCustomerDAO();
		factory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = factory.createEntityManager();
		blCustomerDAO.setEntityManager(entityManager);
		entityManager.getTransaction().begin();
	}
	@After
	public void after() {
		entityManager.getTransaction().commit();
		entityManager.close();
		factory.close();
	}

	@Test
	public void insertCustomerTest() {
		CustomerEntity customer = new CustomerEntity("Mara", "Ionescu", "mara@yahoo.com", "073456");
		blCustomerDAO.insertCustomer( customer);
		int id=customer.getCustomerId();
		CustomerEntity customerVerify=blCustomerDAO.findCustomerByCustomerId(id);
		assertEquals("Mara", customerVerify.getFirstName());
	}
	
	@Test
	public void updateCustomerByCustomerIdTest() {
		CustomerEntity customer = new CustomerEntity(1,"Marta", "Ionescu", "marta@yahoo.com", "073456");
		blCustomerDAO.updateCustomerByCustomerId( customer);
		int id=customer.getCustomerId();
		CustomerEntity customerVerify=blCustomerDAO.findCustomerByCustomerId(id);
		assertEquals("Marta", customerVerify.getFirstName());
	}
	
	@Test
	public void findCustomerByCustomerIdTest(){
		CustomerEntity customer = new CustomerEntity("Ioana", "Menescu", "ioana@yahoo.com", "073756");
		blCustomerDAO.insertCustomer( customer);
		int id=customer.getCustomerId();
		CustomerEntity customerVerify=blCustomerDAO.findCustomerByCustomerId( id);
		assertEquals("Ioana", customerVerify.getFirstName());
	}
	@Test
	public void findAllCustomersTest(){
		CustomerEntity customer = new CustomerEntity("Ioana", "Menescu", "ioana@yahoo.com", "073756");
		blCustomerDAO.insertCustomer( customer);
		List<CustomerEntity> customersList= blCustomerDAO.findAllCustomers();
		assertEquals(1, customersList.size());
	}
	
	@Test
	public void deleteCustomerByCustomerIdTest() {
		CustomerEntity customer = new CustomerEntity("Ioana", "Menescu", "ioana@yahoo.com", "073756");
		blCustomerDAO.insertCustomer( customer);
		int id=customer.getCustomerId();
		blCustomerDAO.deleteCustomerByCustomerId( id);
		assertNull(blCustomerDAO.findCustomerByCustomerId( id));
	}
	@Test
	public void deleteAllCustomersTest() {
		CustomerEntity customer = new CustomerEntity("Ioana", "Menescu", "ioana@yahoo.com", "073756");
		blCustomerDAO.insertCustomer( customer);
		blCustomerDAO.deleteAllCustomers();
		assertEquals(0,blCustomerDAO.findAllCustomers().size());
	}
	
}
