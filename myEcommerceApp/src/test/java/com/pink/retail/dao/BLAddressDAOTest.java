package com.pink.retail.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pink.retail.address.AddressEntity;
import com.pink.retail.address.BLAddressDAO;


public class BLAddressDAOTest {

	BLAddressDAO blAddressDAO;
	EntityManagerFactory factory;
	EntityManager entityManager;
	@Before
	public void before() {
		blAddressDAO = new BLAddressDAO();
		factory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = factory.createEntityManager();
		blAddressDAO.setEntityManager(entityManager);
		//entityManager.getTransaction().begin();
		blAddressDAO.getEntityManager().getTransaction().begin();

	}
	@After
	public void after() {
		//entityManager.getTransaction().commit();
		blAddressDAO.getEntityManager().getTransaction().commit();
		entityManager.close();
		factory.close();
	}
	@Test
	public void insertAddressTest() {
		AddressEntity addressEntity = new AddressEntity("Magheru", "253A", "Bucuresti", "Bucuresti");
		blAddressDAO.insertAddress(addressEntity);
		int id=addressEntity.getAddressId();
		AddressEntity addressrVerify=blAddressDAO.findAddressByID(id);
		assertEquals("Magheru", addressrVerify.getStreetAddress());
	}
	
	@Test
	public void updateAddressTest() {
		AddressEntity addressEntity = new AddressEntity(1,"Mihai Eminescu", "1823A", "Bucuresti", "Bucuresti");
		blAddressDAO.updateAddress(addressEntity);
		int id=addressEntity.getAddressId();
		AddressEntity addressrVerify=blAddressDAO.findAddressByID(id);
		assertEquals("Mihai Eminescu", addressrVerify.getStreetAddress());
	}
	
	@Test
	public void findAddressByIDTest(){
		AddressEntity addressEntity = new AddressEntity("Sperantei", "325C", "Galati", "Galati");
		blAddressDAO.insertAddress(addressEntity);
		int id=addressEntity.getAddressId();
		AddressEntity addressrVerify=blAddressDAO.findAddressByID(id);
		assertEquals("Sperantei", addressrVerify.getStreetAddress());
	}
	@Test
	public void findAllAddressTest(){
		AddressEntity addressEntity = new AddressEntity("Sperantei", "325C", "Galati", "Galati");
		blAddressDAO.insertAddress(addressEntity);
		List<AddressEntity> addressList=blAddressDAO.findAllAddress();
		assertEquals(1, addressList.size());
	}
	
	@Test
	public void deleteByIdAddressTest() {
		blAddressDAO.setEntityManager(entityManager);
		AddressEntity addressEntity = new AddressEntity("Sperantei", "325C", "Galati", "Galati");
		int id=addressEntity.getAddressId();
		blAddressDAO.deleteByIdAddress(id);
		assertNull(blAddressDAO.findAddressByID(id));
	}
	@Test
	public void deleteAllAddresssTest() {
		AddressEntity addressEntity = new AddressEntity("Sperantei", "325C", "Galati", "Galati");
		blAddressDAO.insertAddress(addressEntity);
		blAddressDAO.deleteAllAddress();
		assertEquals(0,blAddressDAO.findAllAddress().size());
	}
}
