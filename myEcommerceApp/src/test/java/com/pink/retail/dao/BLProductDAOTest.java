package com.pink.retail.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pink.retail.product.BLProductDAO;
import com.pink.retail.product.ProductEntity;

public class BLProductDAOTest {
	
	private BLProductDAO productDAO;
	private EntityManagerFactory factory;
	private EntityManager entityManager;
	private ProductEntity product;
	
	@Before
	public void before () {
		productDAO = new BLProductDAO();
		factory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = factory.createEntityManager();
		productDAO.setEntityManager(entityManager);
		entityManager.getTransaction().begin();
		product = new ProductEntity("Laptop i7", "Asus", 25, 3250);
	}

	@After
	public void after() {
		entityManager.getTransaction().commit();
		entityManager.close();
		factory.close();
	}
	
	@Test
	public void getProductByIDTest() {
		productDAO.insertProduct(product);
		int productID = product.getProductID();
		ProductEntity productTest = productDAO.getProductByID(productID);
		assertEquals("Asus", productTest.getBrand());
	}
	
	@Test
	public void getAllProducts() {
		productDAO.insertProduct(product);
		List<ProductEntity> productList = productDAO.getAllProducts();
		assertEquals(1, productList.size());
	}
	
	@Test
	public void deleteProductByIDTest() {
		productDAO.insertProduct(product);
		int productID = product.getProductID();
		productDAO.deleteProductByID(productID);
		assertNull(productDAO.getProductByID(productID));
	}
	
	@Test
	public void deleteAllProductsTest() {
		productDAO.insertProduct(product);
		productDAO.deleteAllProducts();
		List<ProductEntity> productList = productDAO.getAllProducts();
		assertEquals(0, productList.size());
	}
	
	@Test
	public void insertProductTest() {
		productDAO.insertProduct(product);
		int productID = product.getProductID();
		ProductEntity productTest = productDAO.getProductByID(productID);
		assertEquals("Laptop i7", productTest.getName());
	}
	
	@Test
	public void updateProductTest() {
		productDAO.insertProduct(product);
		product = new ProductEntity("Mouse", "Microsoft", 25, 35);
		product.setProductID(1);
		productDAO.updateProduct(product);
		int productID = product.getProductID();
		ProductEntity productTest = productDAO.getProductByID(productID);
		assertEquals("Mouse", productTest.getName());
	}
}
