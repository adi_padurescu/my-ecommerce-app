package com.pink.retail.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pink.retail.order.BLOrderDAO;
import com.pink.retail.order.OrderEntity;


public class BLOrderDAOTest {
	BLOrderDAO blOrderDAO;
	EntityManagerFactory factory;
	EntityManager entityManager;

	@Before
	public void before() {
		BasicConfigurator.configure();
		blOrderDAO = new BLOrderDAO();
		factory = Persistence.createEntityManagerFactory("EcommerceApp");
		entityManager = factory.createEntityManager();
		blOrderDAO.setEntityManager(entityManager);
		entityManager.getTransaction().begin();
	}

	@After
	public void after() {
		entityManager.getTransaction().commit();
		entityManager.close();
		factory.close();
	}

	@Test
	public void insertOrderTest() {
		OrderEntity order = new OrderEntity("20.05.2018", "Pending", "Lei");
		blOrderDAO.insertOrders(order);
		int id = order.getOrderID();
		OrderEntity orderVerify = blOrderDAO.findOrdersByID(id);
		assertEquals("20.05.2018", orderVerify.getOrderDate());
	}

	@Test
	public void updateOrderTest() {
		OrderEntity order = new OrderEntity(1, "20.05.2018", "Pending", "LEI");
		blOrderDAO.updateOrders(order);
		int id = order.getOrderID();
		OrderEntity orderVerify=blOrderDAO.findOrdersByID(id);
		assertEquals("20.05.2018", orderVerify.getOrderDate());
	}


	@Test
	public void findOrdersByIDTest() {
		OrderEntity order = new OrderEntity(0,"19.05.2018", "Processed", "EURO");
		blOrderDAO.insertOrders(order);
		int id = order.getOrderID();
		OrderEntity orderVerify = blOrderDAO.findOrdersByID(id);
		assertEquals("19.05.2018", orderVerify.getOrderDate());
	}

	@Test
	public void findAllOrdersTest() {
		OrderEntity order = new OrderEntity("18.05.2018", "Shipped", "DOLAR");
		blOrderDAO.insertOrders(order);
		List<OrderEntity> ordersList = blOrderDAO.findAllOrders();
		assertEquals(11, ordersList.size());
	}

	@Test
	public void deleteByIdOrdersTest() {
		OrderEntity order = new OrderEntity("18.05.2018", "Shipped", "DOLAR");
		blOrderDAO.insertOrders(order);
		int id = order.getOrderID();
		blOrderDAO.deleteByIdOrders(id);
		assertNull(blOrderDAO.findOrdersByID(id));
	}

	@Test
	public void deleteAllOrdersTest() {
		OrderEntity order = new OrderEntity("18.05.2018", "Shipped", "DOLAR");
		blOrderDAO.insertOrders(order);
		blOrderDAO.deleteAllOrders();
		assertEquals(0, blOrderDAO.findAllOrders().size());
	}
}