package com.pink.retail.money;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.money.Balance;
import com.pink.retail.money.Money;
import com.pink.retail.money.MoneyStock;

public class BalanceTest {
	
	List<Money> restListTest;
	MoneyStock firmMoneyStockTest;
	List<Double> inputListTest;
	Balance aux_for_test;
	
	@Before
	public void Balance() throws IOException
	{
		firmMoneyStockTest=new MoneyStock();
		restListTest=firmMoneyStockTest.getFirmBudget();
		inputListTest=firmMoneyStockTest.initMoneyList();
		aux_for_test=new Balance();
	}
	
	
	@Test
	public void addRestTest()
	{
		aux_for_test.addRest(restListTest, 500, 10);
		assertTrue(restListTest.get(10).getQuantityMoney()==30);
	}
	
	@Test
	public void validationInputMoneyTest()
	{
		assertTrue(aux_for_test.validationInputMoney(inputListTest, 15)==0);
	}
	
	@Test
	public void cancelCommandMoneyTest()
	{
		List<Money> listForTest=new ArrayList<Money>();
		listForTest.add(new Money(200,5));
		aux_for_test.cancelCommandMoney(firmMoneyStockTest, listForTest);
		assertTrue(firmMoneyStockTest.getFirmBudget().get(9).getQuantityMoney()==25);
	}

}
