package com.pink.retail.money;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.money.Currency;
import com.pink.retail.money.Money;

public class CurrencyTest {
	
	List<Money> currenceListTest;
	Currency currenceForTest;
	
	@Before
	public void Currence()
	{
		currenceListTest=new ArrayList<Money>();
		currenceForTest=new Currency();
	}
	
	@Test
	public void chooseCurrenceTest() throws IOException
	{
		assertTrue(currenceForTest.chooseCurrence(1)==4.7);
	}
	
	@Test
	public void changeForeignValueTest()
	{
		assertTrue(currenceForTest.changeForeignValue(10, 10)==100);
	}
	
	@Test
	public void divideNumberInMoneyTest() throws IOException
	{
		currenceForTest.divideNumberInMoney(700, currenceListTest);
		assertTrue(currenceListTest.size()==2);
	}
}
