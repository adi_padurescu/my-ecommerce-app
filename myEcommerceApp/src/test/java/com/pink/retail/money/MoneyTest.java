package com.pink.retail.money;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.money.Money;

public class MoneyTest {

	public Money testMoney;
	
	@Before
	public void Money()
	{
		testMoney=new Money(50,10);
	}
	
	@Test
	public void getValueTest()
	{
		assertTrue(testMoney.getValue()==50);
	}
	
	@Test
	public void setValueTest()
	{
		testMoney.setValue(100);
		assertTrue(testMoney.getValue()==100);
	}
	
	@Test
	public void getQuantityMoneyTest()
	{
		assertTrue(testMoney.getQuantityMoney()==10);
	}
	
	@Test
	public void setQuantityMoneyTest()
	{
		testMoney.setQuantityMoney(20);
		assertTrue(testMoney.getQuantityMoney()==20);
	}
}
