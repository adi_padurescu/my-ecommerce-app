package com.pink.retail.money;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.money.Money;
import com.pink.retail.money.MoneyStock;

public class MoneyStockTest {
	
	MoneyStock moneyStockListTest;
	List<Double> moneyStockListInitTest;
	List<Money> moneyListTest;
	
	@Before
	public void MoneyStock() throws IOException
	{
		moneyStockListTest=new MoneyStock();
		moneyStockListInitTest=new ArrayList<Double>();
		moneyListTest=new ArrayList<Money>();
	}
	
	@Test
	public void getListMoneyStockTest()
	{
		assertTrue(moneyStockListTest.getFirmBudget().size()==11);
	}
	
	@Test
	public void getListInitMoneyStockTest() throws IOException
	{
		moneyStockListInitTest=moneyStockListTest.initMoneyList();
		assertTrue(moneyStockListInitTest.size()==11);
	}
	
	@Test
	public void addMoneyFromListTest()
	{
		moneyStockListTest.addMoneyFromList(moneyStockListTest, 1, 15);
		assertTrue(moneyStockListTest.getFirmBudget().get(0).getQuantityMoney()==35);
	}
	
	@Test
	public void opOnMoneyListTest()
	{
		moneyListTest=moneyStockListTest.getFirmBudget();
		moneyStockListTest.opOnMoneyList(moneyListTest, 2, 10);
		assertTrue(moneyListTest.get(2).getQuantityMoney()==30);
	}
	
	@Test
	public void initialBalanceFirmTest()
	{
		assertTrue(moneyStockListTest.initialBalanceFirm(moneyStockListTest)==17333.2);
	}
	
	@Test
	public void updateFirmBalanceTest()
	{
		moneyListTest.add(new Money(500,20));
		moneyStockListTest.updateFirmBalance(moneyStockListTest, moneyListTest);
		assertTrue(moneyStockListTest.getFirmBudget().get(10).getQuantityMoney()==40);
	}
}
