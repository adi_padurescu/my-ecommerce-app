package com.pink.retail.money;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.pink.retail.money.BLIMoneyReader;
import com.pink.retail.money.BLMoneyReader;
import com.pink.retail.money.Money;

public class BLMoneyReaderTest {

	public BLIMoneyReader readerTest;
	
	@Before
	public void BLMoneyReader()
	{
		readerTest=BLMoneyReader.getInstance();
	}
	@Test
	public void readListMoneyFirmTest() throws IOException
	{
		List<Money> testMoneyList=new ArrayList<Money>();
		testMoneyList=readerTest.readListMoneyFirm("src/main/resources/budget.txt");
		assertTrue(testMoneyList.size()==11);
	}
	@Test
	public void readListMoneyAvailableTest() throws IOException
	{
		List<Double> testMoneyListAv=new ArrayList<Double>();
		testMoneyListAv=readerTest.readListMoneyAvailable("src/main/resources/money.txt");
		assertTrue(testMoneyListAv.size()==11);
	}
}
