Aplicatia mea simuleaza un site de shopping online in care utilizatorii pot achizitiona diferite modele de telefoane si in cantitati diferite.

Pas 1: Cand este rulat programul prima oara este afisata o lista cu produsele disponibile, cantitatile si preturile acestora.
Pas 2: Utilizatorul introduce numarul din dreptul produsului dorit, daca acesta introduce un numar care nu exista sau acel produs are stocul epuizat, utilizatorului i se va cere sa introduca din nou index-ul.
Pas 3: Utilizatorul introduce cate produse doreste, daca numarul de produse pe care il doreste este mai mare decat cel disponibil este rugat sa reintroduca numarul de produse.
Pas 4: Utilizatorului i se afiseaza suma pe care trebuie sa o achite si este intrebat daca mai doreste sa comande si alte produse, caz in care se reiau pasii 1-3 sau merge mai departe sa achite produsele.
Pas 5: Este intrebat utilizatorul cum doreste sa plateasca : cu monede sau bancnote. In functie de ce alege i se va afisa o lista cu valorile bancnotelor/monedelor din care poate selecta. Daca introduce un index care nu exista, acesta va fi rugat sa introduca din nou. Dupa aceea este rugat sa aleaga cu cate bancnote/monede de acea valoare plateste.
Pas 6: I se afiseaza utlizatorului suma pe care a platit-o, respectiv cat mai are de platit si este reluat pasul 5.
Pas 7: Utilizatorului i se afiseaza o factura in care regaseste produsele selectate, respectiv modul in care a platit si este intrebat daca confirma comanda. Daca este confirmata comanda, lista de produse a magazinului este afisata in format actualizati impreuna cu profitul firmei, iar in cazul in care clientul refuza comanda lista cu produse ramane nemodificata.


Structura programului pe clase: 
-Comanda: Am scris tot procesul de facut comanda, incepand de la comandatul de produse si pana la plata completa.
-Plata: Am descris metodele necesare pentru plata unei comenzi: actualizarea listei cu valorile si numarul de bancnote/monede pe care clientul le-a folosit si afisarea listei cu valorile bancnotelor/monedelor.
-Servicii: Am descris metodele necesare pentru achizitionarea unui produs, actualizarea listei cu produse a firmei daca o comanda s-a efectuat cu succes sau a fost anulata.
-Util: Am descris metodele pentru citirea din fisier a produselor si afisarea acestora. 